/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include <stdlib.h>
#include <vector.h>

#include "error.h"
#include "product.h"
#include "treeitem.h"
#include "utils.h"

/** @brief Allocates and itinitalizing TreeItem struct
 *
 *  NOTE: definition and parent pointers are shared, and will not be freed
 *        on ti_free.
 *
 * @param id: string
 * @param name: string
 * @param desc: string
 * @param product_id: string
 * @param definition: product definition of instance
 * @param parent: product definition of parent
 *
 * @return Pointer to new TreeItem or NULL on allocation failure
 */
struct TreeItem* ti_new(char *id,
                         char *name,
                         char *desc,
                         char *product_id,
                         struct ProductDefinition *definition,
                         struct ProductDefinition *parent,
                         struct Product *product)
{
    struct TreeItem *new_ti = calloc(1, sizeof(struct TreeItem));
    if (!new_ti) return NULL;

    new_ti->instance_id = copy_string(id);
    new_ti->instance_name = copy_string(name);
    new_ti->instance_desc = copy_string(desc);
    new_ti->product_id = copy_string(product_id);
    new_ti->master = product;
    new_ti->parent = parent;
    new_ti->definition = definition;

    return new_ti;
}

/** @brief Recursivley frees child treeitems and text fields. Pointers to
 *  Product* structs is freed by their respective owners
 *
 *  @param item: pointer to root element in list
 *  @return void
 */
void ti_free(struct TreeItem *item)
{
    if (!item) return;

    if (item->instance_desc) {
        free(item->instance_desc);
        item->instance_desc = NULL;
    }

    if (item->instance_id) {
        free(item->instance_id);
        item->instance_id = NULL;
    }

    if (item->instance_name) {
        free(item->instance_name);
        item->instance_name = NULL;
    }

    if (item->product_id) {
        free(item->product_id);
        item->product_id = NULL;
    }

    if (item->children) {
        vector_free(&item->children);
        item->children = NULL;
    }

    item->master = NULL;

    free(item);
    item = NULL;
}

/** @brief Append child to parent TreeItem
 *
 *  @param parent: parent TreeItem
 *  @param child: child TreeItem
 *  @return ESUCCESS on successful append, else negative value indicating failure
 */
int ti_append_child(struct TreeItem *parent, struct TreeItem *child)
{
    if (!parent->children) {
        parent->children = vector_new(&ti_free);
        if (!parent->children) return -ENOMEM;
    }

    return vector_append(parent->children, child);
}

/** @brief Combines PRODUCT_DEFINITION, PRODUCT_DEFINITION_FORMATION
 *  and NEXT_ASSEMBLY_USAGE_OCCURRENCE into a product tree
 *
 *  @param stp: step file handle
 *  @return ESUCCESS on success, else errorcode indicating failure
 */
int build_product_tree(Step *stp)
{
    Vector *root = vector_new(&ti_free);
    if (!root) return -ENOMEM;

    int rc = ESUCCESS;
    size_t i = 0;
    struct ProductDefinition *def = NULL;

    /* Recursivley build treeitems */
    while ((def = vector_get(stp->definitions, i))) {
        if (!def->is_child) {
            char *product_id = pr_find_product(stp->formations, def->reference);
            struct Product *product = product_find(stp->products, product_id);
            struct TreeItem *item = ti_new(def->partnumber,
                                            NULL,
                                            def->desc,
                                            product_id,
                                            def,
                                            NULL,
                                            product);
            rc = build_tree_items(stp, def->children, &item);
            if (rc != ESUCCESS) return rc;

            rc = vector_append(root, item);
            if (rc != ESUCCESS) return rc;
        }
        i++;
    }

    rc = vector_shrink(root);
    if (rc!= ESUCCESS) return rc;

    /* Connect allocated resources to step file handle */
    stp->product_tree = root;

    return ESUCCESS;
}

/** @brief Builds a tree of TreeItems. New TreeItems is appended as children
 *  to **items parent TreeItem.
 *
 *  @param stp: step file handle
 *  @param children: pointer to vector of ProductUsage of parent children
 *  @param items: parent tree item
 *  @return ESUCCESS on success, else negative value indicating error
 */
int build_tree_items(Step *stp,
                     Vector *children,
                     struct TreeItem **items)
{
    size_t i = 0;
    int rc = ESUCCESS;
    struct ProductUsage *usage = NULL;
    while ((usage = vector_get(children, i))) {
        struct ProductDefinition *child = pd_find_by_id(stp->definitions, usage->child);
        struct ProductDefinition *parent = pd_find_by_id(stp->definitions, usage->parent);
        char *product_id = pr_find_product(stp->formations, child->reference);
        struct Product *product = product_find(stp->products, product_id);

        struct TreeItem *item = ti_new(usage->id,
                                        usage->name,
                                        usage->desc,
                                        product_id,
                                        child,
                                        parent,
                                        product);
        if (!item) return -ENOMEM;

        if (child->children) {
            rc = build_tree_items(stp, child->children, &item);
            if (rc != ESUCCESS)
                return rc;
        }

        rc = ti_append_child((*items), item);
        if (rc != ESUCCESS) return rc;
        i++;
    }

    rc = vector_shrink((*items)->children);
    return rc;
}

/** @brief Searching for a specific step definition id in vector of
 *  struct TreeItems
 *
 *  @param v: Pointer to vector of struct TreeItems
 *  @param id: string step id
 *  @return Pointer to matching TreeItem, NULL if not found
 */
struct TreeItem* ti_find_id(Vector *v, char* id)
{
    struct TreeItem *item = NULL;
    int i = 0;

    vector_reset(v);
    while ((item = vector_iterate(v))) {
        if (str_cmp(item->definition->step_id, id))
            return item;
        i++;
    }

    return NULL;
}
