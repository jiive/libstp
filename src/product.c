/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include "product.h"
#include "step.h"
#include "step_entity.h"
#include "treeitem.h"
#include "utils.h"


/** @brief Filters relevant entities from step, converts data to proper
 *  datastructures, and builds the PDM tree("bill of material")
 *
 *  @param stp: pointer to parsed step file
 *  @return ESUCCESS on success, eler return code indicating error
 */
int process_product_entities(Step *stp)
{
    /* 1. Populate tree items fron PRODUCT_DEFINITION table, COPY
     * 2. Build a product list of the PRODUCT table
     * 3. Find product reference in PRODUCT_DEFINITION_FORMATION table
     *    and cross reference with product list
     * 4. Find the *one* parent entity in NEXT_ASSEMBLY_USAGE_OCCURENCE
     *    - Assuming each product has exactly one parent
     * 5. Find all children in NEXT_ASSEMBLY_USAGE_OCCURENCE
     */

    struct StepEntity *def_ent = NULL;
    struct StepEntity *form_ent = NULL;
    struct StepEntity *product_ent = NULL;
    struct StepEntity *usage_ent = NULL;

    int rc = ESUCCESS;

    /* Filter relevant info */
    rc = se_find_type(stp->data_first, "PRODUCT_DEFINITION", &def_ent);
    if (rc != ESUCCESS) goto cleanup;

    rc = se_find_type(stp->data_first, "PRODUCT_DEFINITION_FORMATION", &form_ent);
    if (rc != ESUCCESS) goto cleanup;

    rc = se_find_type(stp->data_first, "PRODUCT", &product_ent);
    if (rc != ESUCCESS) goto cleanup;

    rc = se_find_type(stp->data_first, "NEXT_ASSEMBLY_USAGE_OCCURRENCE", &usage_ent);
    if (rc != ESUCCESS) goto cleanup;

    /* Convert to proper datastructures */
    stp->products = product_convert_entities(product_ent);
    stp->usage = pu_convert_entities(usage_ent);
    stp->formations = pr_convert_entities(form_ent);
    stp->definitions = pd_convert_entities(def_ent);

    rc = establish_relationships(stp);
    if (rc != ESUCCESS) goto cleanup;

    rc = build_product_tree(stp);
    if (rc != ESUCCESS) goto cleanup;

cleanup:
    if (def_ent) {
        se_free(def_ent);
        def_ent = NULL;
    }

    if (product_ent) {
        se_free(product_ent);
        product_ent= NULL;
    }

    if (usage_ent) {
        se_free(usage_ent);
        usage_ent = NULL;
    }

    if (form_ent) {
        se_free(form_ent);
        form_ent = NULL;
    }


    return rc;
}

/** @brief Populates ProductDefinition->children with ProductUsage instances
 *  of children
 *
 *  @param stp: Handle to step file
 *  return ESUCCESS on success, else negative value indicating failure
 */
int establish_relationships(Step *stp)
{
    struct ProductDefinition *def = NULL;
    struct ProductUsage *usage = NULL;
    size_t i = 0;
    int rc = ESUCCESS;

    while ((def = vector_get(stp->definitions, i))) {
        // Find parent in usage
        size_t j = 0;
        while ((usage = vector_get(stp->usage, j))) {
            if (str_cmp(def->step_id, usage->child)) {
                /* Connect child usage to the parent definitions children Vector */
                struct ProductDefinition *parent = pd_find_by_id(stp->definitions, usage->parent);
                rc = pd_append_child(parent, usage);
                if (rc != ESUCCESS) return rc;

                /* Connect child usage occurrence to its definition */
                usage->definition = def;
                def->is_child = true;
            }
            j++;
        }
        i++;
    }

    return ESUCCESS;
}

/** @brief Converts PRODUCT entity list to Product struct list
 *  Allocates and initializing memory
 *
 *  NOTE: This is a copy operation, remember to free with product_free()
 *
 *  @param node: pointer to first element in list
 *  @return Pointer to Vector of struct Product on success, else NULL
 */
Vector* product_convert_entities(struct StepEntity *node)
{
    if (!node) return NULL;

    Vector *products = vector_new(&product_free);
    if (!products) return NULL;

    struct Product *new_product = NULL;

    do {
        new_product = product_new(node);
        if (!new_product) {
            debug_print("Memory allocation error, aborting");
            break;
        }

        int rc = vector_append(products, new_product);
        if (rc != ESUCCESS) {
            debug_printf("Unable to convert entity #%s, RC%d\n", node->id, rc);
            /* Break loop and return the resources that have already been
             * allocated */
            break;
        }
    } while ((node = node->next));

    return products;
}

/** @brief Converts PRODUCT entity to Product struct
 *  Allocates and initializing memory
 *
 *  NOTE: This is a copy operation, remember to free with product_free()
 *
 *  @param entity: pointer to first element in list
 *  @return Pointer to new struct on success, else NULL
 */
struct Product* product_new(struct StepEntity *entity)
{
    Vector *values = entity->values->data;
    struct Product *prod = calloc(1, sizeof(struct Product));
    if (!prod) return NULL;

    struct StepField *part = vector_get(values, 0);
    if (!part) return NULL;

    struct StepField *name = vector_get(values, 1);
    if (!name) return NULL;

    struct StepField *desc = vector_get(values, 2);
    if (!desc) return NULL;

    prod->partnumber = copy_string(part->data);
    prod->name = copy_string(name->data);
    prod->desc = copy_string(desc->data);
    prod->step_id = copy_string(entity->id);

    return prod;
}

/** @brief Deallocates Product struct
 *
 * @param node: pointer to Product struct to be deallocates
 * @return void
 */
void product_free(struct Product *node)
{
    if (!node) return;

    if (node->desc) {
        free(node->desc);
        node->desc = NULL;
    }

    if (node->name) {
        free(node->name);
        node->name = NULL;
    }

    if (node->partnumber) {
        free(node->partnumber);
        node->partnumber = NULL;
    }

    if (node->step_id) {
        free(node->step_id);
        node->step_id = NULL;
    }

    free(node);
    node = NULL;
}

/** @brief Convert single entity of NEXT_ASSEMBLY_USAGE_OCCURENCE to
 * ProductUsage struct
 *
 * NOTE: This is a copy operation, remember to free with pu_free()
 *
 * @param entity: pointer to step entity
 * @return Pointer to new ProductUsage struct on succes, else NULL
 */
struct ProductUsage* pu_new(struct StepEntity *entity)
{
    if (!entity) return NULL;
    Vector *values = entity->values->data;

    struct ProductUsage *new_pu = calloc(1, sizeof(struct ProductUsage));
    if (!new_pu) return NULL;

    struct StepField *child = vector_get(values, 4);
    if (!child) return NULL;

    struct StepField *desc = vector_get(values, 2);
    if (!desc) return NULL;

    struct StepField *id = vector_get(values, 0);
    if (!id) return NULL;

    struct StepField *name = vector_get(values, 1);
    if (!name) return NULL;

    struct StepField *parent = vector_get(values, 3);
    if (!parent) return NULL;

    new_pu->child = copy_string(child->data);
    new_pu->desc = copy_string(desc->data);
    new_pu->id = copy_string(id->data);
    new_pu->name = copy_string(name->data);
    new_pu->parent = copy_string(parent->data);

    return new_pu;
}

/** @brief Converts linked list of entities of type NEXT_ASSEMBLY_USAGE_OCCURENCE
 * to Vector of ProductUsage structs
 *
 * NOTE: This is a copy operation, remember to free with pu_free()
 *
 * @param node: pointer to first element in list
 * @return Pointer to Vector on success, else NULL
 */
Vector* pu_convert_entities(struct StepEntity *node)
{
    Vector *usage = vector_new(&pu_free);
    if (!usage) return NULL;

    struct ProductUsage *new_pu = NULL;

    do {
        new_pu = pu_new(node);
        if (!new_pu) {
            debug_print("Memory allocation error, aborting");
            break;
        }

        int rc = vector_append(usage, new_pu);
        if (rc != ESUCCESS) {
            debug_printf("Unable to convert entity #%s, RC%d\n", node->id, rc);
            break;
        }
    } while ((node = node->next));

    vector_shrink(usage);

    return usage;
}

/** @brief Deallocates ProductUsage struct
 *
 * @param node: pointer to element to be deallocated
 * @return void
 */
void pu_free(struct ProductUsage *node)
{
    if (!node) return;

    if (node->child) {
        free(node->child);
        node->child = NULL;
    }
    if (node->desc) {
        free(node->desc);
        node->desc = NULL;
    }

    if (node->id) {
        free(node->id);
        node->id = NULL;
    }

    if (node->name) {
        free(node->name);
        node->name = NULL;
    }

    if (node->parent) {
        free(node->parent);
        node->parent = NULL;
    }

    free(node);
    node = NULL;
}

/** @brief Convert single entity of PRODUCT_DEFINITION_FORMATION to
 * ProductReference struct
 *
 * NOTE: This is a copy operation, remember to free with pr_free()
 *
 * @param entity: pointer to step entity
 * @return Pointer to new ProductUsage struct on success, else NULL
 */
struct ProductReference* pr_new(struct StepEntity *entity)
{
    if (!entity) return NULL;
    Vector *values = entity->values->data;

    struct ProductReference *new_ref = calloc(1, sizeof(struct ProductReference));
    if (!new_ref) return NULL;

    struct StepField *id = vector_get(values, 0);
    if (!id) return NULL;

    struct StepField *desc = vector_get(values, 1);
    if (!desc) return NULL;

    struct StepField *product_id = vector_get(values, 2);
    if (!product_id) return NULL;

    new_ref->step_id = copy_string(entity->id);
    new_ref->desc = copy_string(desc->data);
    new_ref->id = copy_string(id->data);
    new_ref->product_id = copy_string(product_id->data);

    return new_ref;
}

/** @brief Deallocates ProductReference struct
 *
 * @param node: pointer to struct to be deallocated
 * @return void
 */
void pr_free(struct ProductReference *node)
{
    if (!node) return;

    if (node->id) {
        free(node->id);
        node->id = NULL;
    }

    if (node->step_id) {
        free(node->step_id);
        node->step_id = NULL;
    }

    if (node->desc) {
        free(node->desc);
        node->desc = NULL;
    }

    if (node->product_id) {
        free(node->product_id);
        node->product_id = NULL;
    }

    free(node);
    node = NULL;
}

/** @brief Converts linked list of entities of type PRODUCT_DEFINITION_FORMATION
 *  to Vector of ProductReference structs
 *
 *  NOTE: This is a copy operation, remember to free with with vector_free()
 *
 *  @param node: pointer to first element in list
 *  @return Pointer to new Vector on success, else NULL
 */
Vector* pr_convert_entities(struct StepEntity *node)
{
    if (!node) return NULL;

    Vector *references = vector_new(&pr_free);
    struct ProductReference *new_ref = NULL;

    do {
        new_ref = pr_new(node);
        if (!new_ref) {
            debug_print("Memory allocation error, aborting");
            break;
        }

        int rc = vector_append(references, new_ref);
        if (rc != ESUCCESS) {
            debug_printf("Unable to convert entity #%s, RC%d\n", node->id, rc);
        }
    } while ((node = node->next));

    vector_shrink(references);

    return references;
}

/** @brief Searches for product id for given reference
 *
 *  NOTE: This is NOT a copy operation, don't free returned string
 *
 *  @param ref: Vector of ProductReference structs
 *  @param id: string with id to search for
 *  @return Pointer to string containing product id if found, else NULL
 */
char* pr_find_product(Vector *ref, char *id)
{
    size_t i = 0;
    struct ProductReference *ref_node = NULL;

    while ((ref_node = vector_get(ref, i))) {
        if (str_cmp(ref_node->step_id, id))
            return ref_node->product_id;
        i++;
    }

    return NULL;
}

/** @brief Convert single entity of PRODUCT_DEFINITION to
 * ProductDefinition struct
 *
 * NOTE: This is a copy operation, remember to free with pr_free()
 *
 * @param entity: pointer to step entity
 * @return Pointer to new ProductUsage struct on succes, else NULL
 */
struct ProductDefinition* pd_new(struct StepEntity *entity)
{
    if (!entity) return NULL;
    Vector *values = entity->values->data;

    struct ProductDefinition *new_def = calloc(1, sizeof(struct ProductDefinition));
    if (!new_def) return NULL;

    struct StepField *desc = vector_get(values, 0);
    if (!desc) return NULL;

    struct StepField *product = vector_get(values, 1);
    if (!product) return NULL;

    struct StepField *reference = vector_get(values, 2);
    if (!reference) return NULL;

    struct StepField *context = vector_get(values, 3);
    if (!context) return NULL;

    new_def->children = NULL;
    new_def->context = copy_string(context->data);
    new_def->desc = copy_string(desc->data);
    new_def->is_child = false;
    new_def->partnumber = copy_string(product->data);
    new_def->reference = copy_string(reference->data);
    new_def->step_id = copy_string(entity->id);

    return new_def;
}

/** @brief Deallocates a ProductDefinitions struct
 *
 * @param node: pointer to ProductDefinitions to be deallocated
 * @return void
 */
void pd_free(struct ProductDefinition *node)
{
    if (!node) return;


    if (node->children) {
        vector_free(&node->children);
        node->children = NULL;
    }

    if (node->context) {
        free(node->context);
        node->context = NULL;
    }

    if (node->desc) {
        free(node->desc);
        node->desc = NULL;
    }

    if (node->partnumber) {
        free(node->partnumber);
        node->partnumber = NULL;
    }

    if (node->reference) {
        free(node->reference);
        node->reference = NULL;
    }

    if (node->step_id) {
        free(node->step_id);
        node->step_id = NULL;
    }

    free(node);
    node = NULL;
}

/** @brief Converts linked list of entities of type PRODUCT_DEFINITION
 *  to linked list of ProductReference structs
 *
 *  NOTE: This is a copy operation, remember to free with pu_free()
 *
 *  @param node: pointer to first element in list
 *  @return Pointer to Vector of ProductDefinitions on success, else NULL
 */
Vector* pd_convert_entities(struct StepEntity *node)
{
    if (!node) return NULL;

    Vector *definitions = vector_new(&pd_free);
    if (!definitions) return NULL;

    struct ProductDefinition *new_def = NULL;

    do {
        new_def = pd_new(node);
        if (!new_def) {
            debug_print("Memory allocation error, aborting");
            break;
        }

        int rc = vector_append(definitions, new_def);
        if (!new_def) {
            debug_printf("Unable to convert entity #%s, RC%d\n", node->id, rc);
            break;
        }
    } while ((node = node->next));

    vector_shrink(definitions);

    return definitions;
}

/** @brief Adds child to product definition
 *
 * @param def: pointer to ProductDefinition struct
 * @param child: pointer to child instance
 * @return ESUCCESS on success, else errorcode indicating failure
 */
int pd_append_child(struct ProductDefinition *def, struct ProductUsage *child)
{
    if (!def->children) {
        def->children = vector_new(NULL);
        if (!def->children)
            return -ENOMEM;
    }

    return vector_append(def->children, child);
}

/** @brief Finds specific ProductDefinition based on step entity id
 *
 *  @param definitions: pointer to Vector of ProductDefinitions
 *  @param id: step id to search for
 *  @return Pointer to ProductDefinitions truct on success, else NULL
 */
struct ProductDefinition* pd_find_by_id(Vector *definitions, char* id)
{
    struct ProductDefinition* def = NULL;
    size_t i = 0;
    while ((def = vector_get(definitions, i))) {
        if (str_cmp(def->step_id, id))
            return def;
        i++;
    }

    return NULL;
}

/** @brief Finds specific Product based on step entity id
 *
 *  @param products: pointer to Vector of ProductDefinitions
 *  @param id: step id to search for
 *  @return Pointer to Product struct on success, else NULL
 */
struct Product* product_find(Vector *products, char* id)
{
    struct Product *node = NULL;

    vector_reset(products);
    while ((node = vector_iterate(products))) {
        if (str_cmp(node->step_id, id)) {
            break;
        }
    }

    return node;
}
