/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include <stdlib.h>
#include "step.h"
#include "step_field.h"
#include "step_list.h"

/** @brief Allocates a new list struct
 *
 * @return struct StepList: Pointer to allocated struct
 */
struct StepList* sl_new()
{
    struct StepList* list = calloc(1, sizeof(struct StepList));
    list->field = NULL;
    list->next = NULL;
    return list;
}

/** @brief Allocates a new list struct
 *
 * @return struct StepList: Pointer to allocated struct
 */
int sl_free(struct StepField* list)
{
    if (!list) return -EINVAL;
    if (list->type != LIST) return -EINVAL;

    if (!list->data) {
        free(list);
        list = NULL;
        return ESUCCESS;
    }

    struct StepList* node = list->data;
    struct StepList* prev_node = NULL;
    do {
        if (prev_node)
            free(prev_node);

        if (node->field->type == STRING) {
            sf_free(node->field);
        } else if (node->field->type == LIST){
            sl_free(node->field);
            node->field = NULL;
        }

        prev_node = node;
    } while ((node = node->next));

    if (prev_node) {
        free(prev_node);
        prev_node = NULL;
    }

    free(list);
    list = NULL;

    return ESUCCESS;
}

/** @brief Finds the last node in a list
 *
 * @param list: List to find last element of
 * @return struct StepList*: Pointer to last element on success
 */
struct StepList* sl_last(struct StepField* list)
{
    if (list->type != LIST) return NULL;

    struct StepList* node = list->data;
    while (node->next != NULL)
        node = node->next;

    return node;
}

/** @brief Appends a field to the last node in a list
 *
 * @param list: pointer to root-node in  list
 * @param field: pointer to field to be appended to list
 * @return void: None
 */
bool sl_append(struct StepField* list, struct StepField* field)
{
    if (!field) return false;
    if (list->type != LIST) return false;

    if (list->data == NULL) {
        list->data = sl_new();
    }

    struct StepList* parent = list->data;

    // Set field-value in root-node if root-node is the only element
    if (parent->field == NULL && parent->next == NULL)
        parent->field = field;
    else {
        struct StepList* last = sl_last(list);
        last->next = sl_new();
        last = last->next;
        last->field = field;
    }

    return true;
}

/** @brief Get the n:th node in a list
 *
 * First node is 0:th node
 * NOTE: This is NOT a copy operaiton, do NOT free pointer
 *
 * @param list: pointer to list head node
 * @param idx: Integer index value
 * @return struct StepList*: Pointer to the element at index, null if out of range
 */
struct StepList* sl_index(struct StepField* list, size_t idx)
{
    size_t i = 0;
    if (!list || (list->type != LIST))
        return NULL;

    struct StepList* cursor = list->data;

    while (true) {
        if (idx == i)
            return cursor;

        if (cursor->next == NULL)
            return NULL;

        cursor = cursor->next;
        i++;
    }

    return NULL;
}

/*
int sl_copy(struct StepField* list, struct StepField** out)
{
    if (list->type != LIST) return -EINVAL;

    if (!*out)
        *out = sf_new(LIST);

    int rc = ESUCCESS;
    struct StepList* node = list->data;
    struct StepField* nfield = NULL;
    char* newstr = NULL;
    int len = 0;

    while (true) {
        if (node->field->type == STRING) {
            len = strlen(node->field->data) + 1;
            newstr = calloc(len, sizeof(char));
            memcpy(newstr, node->field->data, len);
            nfield = sf_new(STRING);
            nfield->data = newstr;
        } else {
            nfield = sf_new(LIST);
            sl_copy(node->field, &nfield);
        }
        sl_append(*out, nfield);

        if (!node->next)
            break;

        node = node->next;
    }

    return rc;
}
*/
