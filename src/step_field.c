/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <stdlib.h>
#include <vector.h>

#include "step_field.h"
#include "utils.h"

/** @brief Allocates a new field struct
 *
 * @param type: StepType of field
 * @return struct StepField*: Pointer to allocated field
 */
struct StepField* sf_new(StepType type) {
    struct StepField* field = calloc(1, sizeof(struct StepField));
    if (!field)
        return NULL;

    field->type = type;
    field->data = NULL;

    return field;
}

/** @brief Deletes field and frees allocated resources
 *
 * @param type: StepType of field
 * @return void
 */
void sf_free(struct StepField* field)
{
    if (field->type == LIST) {
        Vector *v = field->data;
        vector_free(&v);
        field->data = NULL;
    } else {
        free(field->data);
        field->data = NULL;
    }
    free(field);
    field = NULL;
}

/** @brief Allocates a new field struct and copies original
 *
 * @param orig: original StepField to copy
 * @param out: copy
 * @return void
 */
void sf_copy(struct StepField *orig, struct StepField **out)
{
    struct StepField *new_field = sf_new(orig->type);

    switch (orig->type) {
    case STRING:
        new_field->data = copy_string(orig->data);
        break;

    case LIST:
        new_field->data = vector_copy(orig->data);
        break;

    default:
        free(new_field);
        new_field = NULL;
        break;
    }

    (*out) = new_field;
}
