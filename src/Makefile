TARGET=stepreader
IDIR =../include
CC=cc
GTKLIB=`pkg-config --cflags --libs gtk+-3.0` `pkg-config --cflags --libs gmodule-2.0`
CFLAGS=-g -I$(IDIR) -Wall -std=c99
LD_FLAGS=-pthread -export-dynamic -rdynamic
GUI_LDFLAGS=$(GTKLIB) $(LDFLAGS)
ODIR=obj
LDIR =../lib

LIBS=

_DEPS = step.h tokenstream.h inputstream.h error.h utils.h step_list.h \
		step_entity.h step_field.h product.h treeitem.h vector.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

# Generic objects
_OBJ = step.o tokenstream.o inputstream.o error.o utils.o step_list.o \
	   step_entity.o step_field.o product.o treeitem.o vector.o

# CLI specific objects
_OBJ_CLI = $(_OBJ) cli.o

OBJ_CLI = $(patsubst %,$(ODIR)/%,$(_OBJ_CLI))

cli_$(TARGET): $(OBJ_CLI)
	LANG=C $(CC) -o $@ $^ $(CFLAGS) $(LIBS) $(LDFLAGS)

$(ODIR)/%.o: %.c $(DEPS)
	LANG=C $(CC) -c -o $@ $< $(CFLAGS) $(GTKLIB)

.PHONY: clean memcheck

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~

memcheck:
	@echo "Running valgrind..."
	@valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all \
		-v --log-file=valgrind.log \
		./cli_stepreader ../testfiles/nist_ctc_01_asme1_ap203.stp
	@echo "Valgrind log written to valgrind.log"
