/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "tokenstream.h"

/** @brief Opens a tokenstream and sets default values.
 *
 * NOTE: Don't forget to call tstream_close to free resources
 * '
 * @param filename: Relative or absolute path to file to be read.
 * @return On success, pointer to tokenstream handle. Returns NULL on failure.
 */
TStream* tstream_open(const char* filename) {
    TStream* ts = calloc(1, sizeof(TStream));
    if (!ts) {
        debug_print("Unable to allocate sufficient memory for tokenstream");
        return NULL;
    }

    ts->is = istream_open(filename);
    if (!ts->is) {
        debug_printf("Error opening file %s\n", filename);
        tstream_close(ts);
        return NULL;
    }

    ts->bufsize = TS_BUFFER_SIZE;
    ts->buffer = calloc(ts->bufsize, sizeof(char));
    if (!ts->buffer) {
        debug_print("Unable to allocate token buffer");
        tstream_close(ts);
        return NULL;
    }

    ts->current = NULL;

    return ts;
}

/** @brief Closes an tokenstream and deallocates memory
 *
 * @param ts: Handle to tokenstream to be closed
 * @return: On success, true. On error NULL.
 */
int tstream_close(TStream* ts) {
    if (!ts) {
        return 1;
    }

    if (ts->is) {
        istream_close(ts->is);
    }

    if (ts->current) {
        free(ts->current);
        ts->current = NULL;
    }

    if (ts->buffer) {
        free(ts->buffer);
        ts->buffer = NULL;
    }

    free(ts);
    ts = NULL;

    return 1;
}

/** @brief Gets the next token
 *
 * NOTE: Don't forget to free resources
 *
 *  @param ts: Handle to tokenstream
 *  @return char*: next token in stream
 */
char* tstream_next(TStream* ts) {
    char* token;

    if (ts->current) {
        token = ts->current;
        ts->current = NULL;
    } else {
        token = tstream_get_token(ts);
    }

    return token;
}

/** @brief This function peeks the next token
 *
 * NOTE: Never free peeked strings
 *
 *  @param ts: Handle to tokenstream
 *  @return char*: next token in stream
 */
char* tstream_peek(TStream* ts) {
    if (!ts->current)
        ts->current = tstream_next(ts);

    return ts->current;
}


/** @brief Internal function! Don't call directly
 *
 *  See \link tstream_next(TStream* ts) \endlink instead
 *  Builds token from input stream
 *
 *  @param ts: Handle to tokenstream
 *  @return char*: token
 */
char* tstream_get_token(TStream* ts) {
    size_t len = 0;
    if (tstream_is_delim(ts, istream_peek(ts->is))) {
        len = tstream_get_delim(ts);
    } else {
        len = tstream_build_token(ts);
    }

    if (len == 0)
        return NULL;

    char* token = calloc(len+1, sizeof(char));
    strncpy(token, ts->buffer, len);
    // Clean buffer
    memset(ts->buffer, '\0', len);

    return token;
}

/** @brief Builds token
 *
 *  @param ts: pointer to tokenstream
 *  @return size_t: length of string, token in ts->buffer
 */
size_t tstream_build_token(TStream* ts) {
    size_t i = 0;
    while (0 != istream_peek(ts->is)){
        if (i == ts->bufsize-1) {
            int rc = tstream_increase_buffer(ts);
            if (rc != ESUCCESS) {
                return 0;
            }
        }

        if (tstream_is_delim(ts, istream_peek(ts->is)))
            break;

        char ch = istream_next(ts->is);
        ts->buffer[i] = ch;
        i++;
    }

    return i;
}

/** @brief Builds token out of single delimiter
 *
 *  @param ts: pointer to tokenstrean
 *  @return size_t: length of string, token in ts buffer
 */
size_t tstream_get_delim(TStream* ts) {
    if (ts->bufsize < 2) {
        int rc = tstream_increase_buffer(ts);
        if (rc != ESUCCESS) {
            return 0;
        }
    }

    char ch = istream_next(ts->is);
    if (ch == 0)
        return 0;

    ts->buffer[0] = ch;
    return 2;
}

/** @brief Check if char is of delimiter type specified in Step/ISO standard
 *
 *  @param ts: File handle
 *  @param ch: character to check
 *  @return bool: returns true character matches any of the delimiters
 */
bool tstream_is_delim(TStream* ts, const char ch) {
    if (ch == 0)
        return false;

    if (istream_in_string(ts->is))
        return ch == DELIM_QUOTE[0];
    else
        return strchr(DELIMS, ch) != NULL ? true: false;
}

/** @brief Increases the size of the token buffer
 *
 *  Reallocates buffer with new size = bufsize += TS_BUFFER_SIZE
 *
 *  @param ts: Pointer to tokenstream
 *  @return ESUCCESS or error code
 */
int tstream_increase_buffer(TStream* ts) {
    //Allocate new buffer and verify
    size_t new_bufsize = ts->bufsize + TS_BUFFER_SIZE;
    char* new_buffer = calloc(new_bufsize, sizeof(char));
    if (!new_buffer) {
        debug_print("Failed to increase token buffer size");
        return -ENOMEM;
    }

    memcpy(new_buffer, ts->buffer, ts->bufsize);

    // Free old and replace with new
    free(ts->buffer);
    ts->buffer = new_buffer;
    ts->bufsize = new_bufsize;

    return ESUCCESS;
}
