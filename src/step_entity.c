/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include <stdlib.h>
#include "step.h"
#include "step_entity.h"
#include "utils.h"


/** @brief Allocates a new entity and sets initial values
 *
 * NOTE: - next pointer is initialised to NULL.
 *       - usage of **out is undefined if return code is not ESUCCESS
 *       - id and type params are not copied, caller should not free them
 *
 * @param out: pointer to newly initialised entity
 * @param id: character string for id field
 * @param type: character string for type field
 * @param values: struct StepEntity of type LIST, holding the values of the entity
 * @return ESUCCESS on success, -ENOMEM on failure to allocate new entity
 */
int se_new(struct StepEntity** out, char* id, char* type, struct StepField* values) {
    int rc = ESUCCESS;
    struct StepEntity* new_ent = calloc(1, sizeof(struct StepEntity));
    if (new_ent) {
        new_ent->id = id;
        new_ent->type = type;
        new_ent->values = values;
        new_ent->next = NULL;
        *out = new_ent;
    } else {
        rc= -ENOMEM;
    }

    return rc;
}

// int se_push(struct StepEntity* first, struct StepEntity* node);

/** @brief Allocates a new entity and sets initial values
 *
 * @param last: pointer to last element in list
 * @param node: struct StepEntity to be appended to list
 * @return ESUCCESS on success, -EINVAL if last is not last element
 */
int se_append(struct StepEntity** last, struct StepEntity* node) {
    if ((*last) == NULL) {
        (*last) = node;
    } else if ((*last)->next != NULL) {
        return -EINVAL;
    } else {
        (*last)->next = node;
        (*last) = (*last)->next;
    }

    return ESUCCESS;
}

/** @brief Recursivly frees StepEntities
 *
 * @param ent: struct StepEntity to be freed
 * @return void
 */
void se_free(struct StepEntity* ent) {
    if (!ent) return;

    struct StepEntity *node = ent;
    struct StepEntity *prev = NULL;

    do {
        if (str_cmp(node->id, "6645"))
            printf("break");

        if (prev) {
            free(prev);
            prev = NULL;
        }

        if (node->id) {
            free(node->id);
            node->id = NULL;
        }

        if (node->type) {
            free(node->type);
            node->type = NULL;
        }

        if (node->values) {
            sf_free(node->values);
            node->values = NULL;
        }

        prev = node;
    } while ((node = node->next));

    if (prev) {
        free(prev);
    }
}

/** @brief Find entities of a specific type
 *
 *  NOTE: This function returns COPIES of each entity. Remember to call se_free
 *        on first element to free memory
 *
 * @param first: First entity in list
 * @param type: Type to look for
 * @param out: Pointer to recieve StepEntity
 * @return ESUCCESS on success, negative value on error
 *         List of entities in out parameter, NULL on error
 */
int se_find_type(struct StepEntity* first, char* type, struct StepEntity** out)
{
    if (!first) return -EINVAL;

    struct StepEntity* copy_first = NULL;
    struct StepEntity* node = first;
    struct StepEntity* copy = NULL;
    struct StepEntity* prev = NULL;

    while (true) {
        if (str_cmp(node->type, type)) {
            copy = se_copy(node);
            if (!copy_first)
                copy_first = copy;

            if (prev) {
                prev->next = copy;
                prev = prev->next;
            }
            else
                prev = copy;
        }

        if (!node->next)
            break;

        node = node->next;
    }

    *out = copy_first;

    return ESUCCESS;
}

/** @brief Creates a new entity and copies all data from the original entity
 *
 * @param orig: struct StepEntity to copy
 * @return Pointer to new copy of orig, NULL on failure
 */

struct StepEntity* se_copy(struct StepEntity* orig)
{
    if (!orig) return NULL;

    struct StepEntity *new_ent = NULL;
    struct StepField *values = NULL;
    char* id = copy_string(orig->id);
    char* type = copy_string(orig->type);

    sf_copy(orig->values, &values);

    se_new(&new_ent, id, type, values);

    return new_ent;
}

/** @brief Returns pointer to first entity in list with id
 *
 * NOTE: Does not copy,  DO NOT FREE POINTER as this will break the link chain
 *
 * @param first: first element in li
 * @param id: id to search for
 * @return Pointer existing nod in list, NULL on failure
 */
struct StepEntity* se_find_by_id(struct StepEntity *first, char* id)
{
    if (!first) return NULL;

    struct StepEntity *node = first;
    do {
        if (str_cmp(node->id, id))
            return node;
    } while ((node = node->next));

    return NULL;
}
