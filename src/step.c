/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "product.h"
#include "step.h"
#include "step_entity.h"
#include "step_field.h"
#include "tokenstream.h"
#include "treeitem.h"
#include "utils.h"

/** @brief Opens step file
 *  Reads whole file to RAM
 *
 * @param stp: pointer to step handle
 * @param filename: Relative or absolute path to file to be read.
 * @return On success, ESUCCESS. A pointer to a valid step file handle is passed in stp.
 *          stp is set to NULL on error, and a returcode indicating what went wrong is returned.
 */
int step_open(Step** stp, const char* filename)
{
    *stp = calloc(1, sizeof(Step));
    if (!*stp)
        return -ENOMEM;

    (*stp)->ts = tstream_open(filename);
    if (!(*stp)->ts) {
        free(*stp);
        return -ENOENT;
    }

    (*stp)->data_first = NULL;
    (*stp)->data_last = NULL;
    (*stp)->definitions = NULL;
    (*stp)->formations = NULL;
    (*stp)->header = NULL;
    (*stp)->num_entities = 0;
    (*stp)->product_tree = NULL;
    (*stp)->products = NULL;
    (*stp)->tree_items = NULL;
    (*stp)->usage = NULL;

    return ESUCCESS;
}

/** @brief Closes a step handle and deallocates resources
 *
 * @param stp: Handle to step file
 * @return None
 */
void step_close(Step** stp)
{
    // If termination is premature, print some debugging info, account for \r\n and eof
    if ((*stp)->ts->is->cursor < (*stp)->ts->is->filesize - 3) {
        debug_printf(
                "Error might have occured, parsing stopped att line %d, col %d, cursor pointer at %zu\n",
                (*stp)->ts->is->cline,
                (*stp)->ts->is->ccol,
                (*stp)->ts->is->cursor);
    }

    if ((*stp)->ts) {
        tstream_close((*stp)->ts);
        (*stp)->ts = NULL;
    }

    if ((*stp)->data_first) {
        se_free((*stp)->data_first);
        (*stp)->data_first = NULL;
        (*stp)->data_last = NULL;
    }

    (*stp)->data_last = NULL;

    if ((*stp)->products) {
        vector_free(&(*stp)->products);
        (*stp)->products = NULL;
    }

    if ((*stp)->definitions) {
        vector_free(&(*stp)->definitions);
        (*stp)->definitions = NULL;
    }

    if ((*stp)->formations) {
        vector_free(&(*stp)->formations);
        (*stp)->formations = NULL;
    }

    if ((*stp)->header) {
        vector_free(&(*stp)->header);
        (*stp)->header = NULL;
    }

    if ((*stp)->usage) {
        vector_free(&(*stp)->usage);
        (*stp)->usage = NULL;
    }

    if ((*stp)->tree_items) {
        vector_free(&(*stp)->tree_items);
        (*stp)->tree_items = NULL;
    }

    if ((*stp)->product_tree) {
        vector_free(&(*stp)->product_tree);
        (*stp)->product_tree = NULL;
    }

    free((*stp));
    (*stp) = NULL;
}

/** @brief Parses header section of step file
 *
 * NOTE: All resources allocated are linked to step file handle stp->header;
 *       Remeber to use step_close() free all resources.
 *
 * @param stp: Handle to step file
 * @return ESUCCESS on success, else error code
 */
int step_read_header(Step* stp)
{
    int rc = ESUCCESS;
    char* keyword = NULL;
    struct StepEntity* node = NULL;
    Vector *entities = NULL;

    // Sanity check
    if (step_expect(stp, "HEADER") != ESUCCESS) return -EUNEXP;
    if (step_expect(stp, DELIM_LINEEND) != ESUCCESS) return -EUNEXP;

    entities = vector_new(&se_free);
    do {
        keyword = tstream_next(stp->ts);
        if (str_cmp(keyword, "ENDSEC"))
            break;
            //free(keyword);
            //keyword = NULL;
            //break;

        rc = step_header_entity(stp, keyword, &node);
        if (rc == ESUCCESS) {
            vector_append(entities, node);
        } else {
            goto cleanup;
        }

        if (step_expect(stp, DELIM_LINEEND) != ESUCCESS) return -EUNEXP;

    } while (true);

    // Sanity check
    if (step_expect(stp, DELIM_LINEEND) != ESUCCESS) return -EUNEXP;

    vector_shrink(entities);
    stp->header = entities;

cleanup:
    free(keyword);
    keyword = NULL;

    return rc;
}

/** @brief Parses entity in header and build list of values
 *
 * Saves data in database
 *
 * @param stp: Handle to step file
 * @param name: Name of enitity
 * @param out: List of values in entity
 * @return ESUCCESS on success, else errorcode
 */
int step_header_entity(Step* stp, char* name, struct StepEntity** out)
{
    struct StepField* values;
    int rc = step_get_list(stp, &values);

    if (rc == ESUCCESS) {
        struct StepEntity* ent = NULL;
        /* Allocate empty id string since header entities don't have id:s */
        char* id = generate_string(0, '\0');

        se_new(&ent, id, name, values);
        (*out) = ent;
    } else {
        debug_print("Error while parsing header entity");
    }

    return rc;
}

/** @brief Parses data section of step file
 *
 * Data is saved in linked list in stp called data. Memory structure in each
 * node is:
 *
 * |-----------------|
 * | ID : String     |
 * |-----------------|
 * | Name : String   |
 * |-----------------|
 * | Values : List   |
 * | Vector | Val1   |
 * |        | Val2   |
 * |        | Val3   |
 * |        | ...    |
 * |-----------------|
 *
 * @param stp: Handle to step file
 * @return ESUCCESS on sucessful parsing, else errorcode
 */
int step_read_data(Step* stp)
{
    if (step_expect(stp, "DATA") != ESUCCESS) return -EUNEXP;
    if (step_expect(stp, DELIM_LINEEND) != ESUCCESS) return -EUNEXP;

    int rc = ESUCCESS;
    struct StepField* id = NULL;
    struct StepField* entity_name = NULL;
    struct StepField* values = NULL;
    struct StepEntity* entity = NULL;
    struct StepEntity* first = NULL;
    struct StepEntity* last = NULL;
    char* token = NULL;
    char* peeked = NULL;
    size_t tlen = 0;
    size_t counter = 0;

    do {
        if (token) {
            free(token);
            token = NULL;
        }

        peeked = tstream_peek(stp->ts);
        tlen = strlen(peeked);
        if (strncmp("ENDSEC", peeked, tlen) == 0)
            break;

        rc = step_get_id(stp, &id);
        if (rc != ESUCCESS) goto cleanup;

        rc = step_expect(stp, DELIM_ASSIGNMENT);
        if (rc != ESUCCESS) goto cleanup;

        peeked = tstream_peek(stp->ts);
        tlen = strlen(peeked);
        if (strncmp(DELIM_LISTSTART, peeked, tlen) == 0) {
            // Create a dummy name if we have encountered an anonymous entity
            rc = step_create_dummy_name(&entity_name);
            if (rc != ESUCCESS) goto cleanup;

            rc = step_get_line(stp, &values);
            if (rc != ESUCCESS) goto cleanup;
        } else {
            rc = step_get_token(stp, &entity_name);
            if (rc != ESUCCESS) goto cleanup;

            rc = step_get_list(stp, &values);
            if (rc != ESUCCESS) goto cleanup;
        }

        rc = se_new(&entity, id->data, entity_name->data, values);
        if (rc != ESUCCESS) goto cleanup;

        se_append(&last, entity);
        if (!first)
            first = last;

        // Free struct StepFields but not ->data pointer as those are ownd by
        // struct StepEntity now and will be freed on step_close call
        // (which calls se_free)
        free(id);
        free(entity_name);
        id = NULL;
        entity_name = NULL;
        values = NULL;

        token = tstream_next(stp->ts);
        tlen = strlen(token);
        ++counter;
    } while (strncmp(token, DELIM_LINEEND, tlen) == 0);

    // Attach allocated resources to stp-handle
    stp->num_entities = counter;
    stp->data_first = first;
    stp->data_last = last;

cleanup:

    if (id) {
        free(id);
        id = NULL;
    }

    if (entity_name) {
        free(entity_name);
        entity_name = NULL;
    }

    if (values) {
        sf_free(values);
        values = NULL;
    }

    if (token) {
        free(token);
        token = NULL;
    }

    return rc;
}


/** @brief Main parse function
 *
 * @param stp: Step file handle
 * @return ESUCCESS on successful parsing, else errorcode.
 */
int step_parse(Step* stp)
{
    int rc = ESUCCESS;

    if (!step_verify_iso(stp)) {
        debug_print("This is not a valid step file");
        return -EINVAL;
    }

    rc = step_read_header(stp);
    if (rc != ESUCCESS) {
        debug_print("Error parsing header");
        return rc;
    }

    rc = step_read_data(stp);
    if (rc != ESUCCESS) {
        debug_print("Error parsing data section");
        return rc;
    }

    // Verify that we have reached the end

    rc = step_expect(stp, "ENDSEC");
    if (rc != ESUCCESS) {
        debug_print("Expected ENDSEC tag after datasection, possible parser error\n");
        return rc;
    }

    rc = step_expect(stp, DELIM_LINEEND);
    if (rc != ESUCCESS) {
        debug_print("Expected line end after datasection, incomplete file?\n");
        return rc;
    }

    rc = step_expect(stp, "END-ISO-10303-21");
    if (rc != ESUCCESS) {
        debug_print("Unexpected end after datasection\n!\n");
        return rc;
    }

    rc = step_expect(stp, DELIM_LINEEND);
    if (rc != ESUCCESS) {
        debug_printf("Unexpected end of file, RC%d", rc);
        return rc;
    }

    rc = process_product_entities(stp);
    if (rc != ESUCCESS) {
        debug_print("Error while building product tree");
        return rc;
    }

    return rc;
}

/** @brief Verify the first token of a step file
 *
 * @param stp: File handle
 * @return On success true, false on failure.
 */
bool step_verify_iso(Step* stp)
{
    char iso_code[] = "ISO-10303-21";
    if (step_expect(stp, iso_code) == ESUCCESS)
        if (step_expect(stp, DELIM_LINEEND) == ESUCCESS)
            return true;

    return false;
}

/** @brief Reads all characters to the EOL token
 *
 * @param stp: File handle
 * @param out: pointer to struct StepField*
 * @return ESUCCESS on success, else errorcode.
 */
int step_get_line(Step* stp, struct StepField** out)
{
    int rc = ESUCCESS;
    char* token = NULL;
    char* peeked = NULL;
    char* line = calloc(1, 1); //Start with empty string
    size_t llen = 0, tlen = 0;

    // Read tokens until we hit a line-end delimiter ';'
    while (true) {
        peeked = tstream_peek(stp->ts);
        tlen = strlen(peeked);

        if(strncmp(peeked, DELIM_LINEEND, tlen) == 0)
            break;

        peeked = NULL;
        token = tstream_next(stp->ts);
        tlen = strlen(token);

        llen = strlen(line);
        line = realloc(line, llen+tlen+1);
        if (!line) {
            rc = -ENOMEM;
            goto cleanup;
        }
        line = strncat(line, token, tlen);
        free(token);
        token = NULL;
    }

    struct StepField* field = sf_new(STRING);
    field->data = line;
    *out = field;

cleanup:
    if (token) {
        free(token);
        token = NULL;
    }

    return rc;
}

/** @brief Reads value within string delimiters
 *
 * NOTE: This function deallocates all memory on failure and returns NULL ptr
 *
 * @param stp: File handle
 * @param out: field value of string type
 * @return ESUCCESS on success, else errorcode
 */
int step_get_string(Step* stp, struct StepField** out)
{
    if (step_expect(stp, DELIM_QUOTE) != ESUCCESS) return -EUNEXP;

    char* val = NULL;
    char* peeked = tstream_peek(stp->ts);
    size_t len = strlen(peeked);
    if (strncmp(peeked, DELIM_QUOTE, len) == 0) {
        // Value is empty string ''
        val = calloc(1, 1);
    } else {
        val = tstream_next(stp->ts);
    }

    if (step_expect(stp, DELIM_QUOTE) != ESUCCESS) {
        free(val);
        val = NULL;
        *out = NULL;
        return -EUNEXP;
    }

    struct StepField* field = sf_new(STRING);
    field->data = val;
    *out = field;

    return ESUCCESS;
}

/** @brief Parses an entity id
 *
 * @param stp: File handle
 * @param out: field of string type
 * @return ESUCCESS on success, else errorcode
 */
int step_get_id(Step* stp, struct StepField** out)
{
    if (step_expect(stp, DELIM_HASH) != ESUCCESS) return -EUNEXP;

    step_get_token(stp, out);

    return ESUCCESS;
}

/** @brief Parses to next delimiter
 *
 * @param stp: File handle
 * @param out: field of string type
 * @return ESUCCESS on success, else errorcode
 */
int step_get_token(Step* stp, struct StepField** out)
{
    char* unit = tstream_next(stp->ts);
    struct StepField* val = sf_new(STRING);
    val->data = unit;
    *out = val;

    return ESUCCESS;
}

/** @brief Reads a vector of values.
 *
 * NOTE: This function deallocates all memory on failure and returns NULL ptr
 *
 * @param stp: File handle
 * @param out: StepField of list type(containing Vector*), NULL pointer on error
 * @return ESUCCESS on success, else errorcode
 */
int step_get_list(Step* stp, struct StepField** out)
{
    if (step_expect(stp, DELIM_LISTSTART) != ESUCCESS) return -EUNEXP;

    int rc = ESUCCESS;
    char* token = NULL;
    size_t len = 0;
    struct StepField* val = NULL;
    Vector *list = vector_new(&sf_free);

    do {
        if (token) free(token);

        token = tstream_peek(stp->ts);
        len = strlen(token);

        if (strncmp(token, DELIM_LISTSTART, len) == 0)
            rc = step_get_list(stp, &val);
        else if (strncmp(token, DELIM_QUOTE, len) == 0)
            rc = step_get_string(stp, &val);
        else if (strncmp(token, DELIM_HASH, len) == 0)
            rc = step_get_id(stp, &val);
        else if (is_alnum(token)) {
            rc = step_get_token(stp, &val);

            // Handle inline function-style entities
            token = tstream_peek(stp->ts);
            if (strncmp(token, DELIM_LISTSTART, strlen(token)) == 0) {
                vector_append(list, val);
                rc = step_get_list(stp, &val);
            }
        }
        else if (strncmp(token, DELIM_LISTEND, len) == 0) {
            // Empty list
            token = tstream_next(stp->ts);
            val = sf_new(STRING);
            val->data = calloc(1, 1);
            break;
        } else
            rc = step_get_value(stp, &val);

        if (rc != ESUCCESS) {
            vector_free(&list);
            goto cleanup;
        }

        vector_append(list, val);

        token = tstream_next(stp->ts);
        len = strlen(token);
    } while (strncmp(token, DELIM_VALUE_SEPARATOR, len) == 0);

    // Sanity check of last token
    if (strncmp(token, DELIM_LISTEND, len) != 0) {
        vector_free(&list);
        rc = -EUNEXP;
        goto cleanup;
    }

    vector_shrink(list);
    *out = sf_new(LIST);
    (*out)->data = list;

cleanup:
    free(token);
    token = NULL;
    return rc;
}


/** @brief Reads a generic value.
 *
 * NOTE: This function deallocates all memory on failure and returns NULL ptr
 *
 * @param stp: File handle
 * @param out: field value of string type
 * @return ESUCCESS on success, else errorcode
 */
int step_get_value(Step* stp, struct StepField** out)
{
    int rc = ESUCCESS;
    char* token = NULL;
    char* peeked = NULL;
    char* value = calloc(1, 1); //Start with empty string
    size_t vlen = 0, tlen = 0;

    while (true) {
        peeked = tstream_peek(stp->ts);
        tlen = strlen(peeked);

        if(strncmp(peeked, DELIM_VALUE_SEPARATOR, tlen) == 0)
            break;
        else if (strncmp(peeked, DELIM_LISTEND, tlen) == 0)
            break;

        peeked = NULL;
        token = tstream_next(stp->ts);
        tlen = strlen(token);

        vlen = strlen(value);
        value = realloc(value, vlen+tlen+1);
        if (!value) {
            rc = -ENOMEM;
            goto cleanup;
        }
        value = strncat(value, token, tlen);
        free(token);
        token = NULL;
    }


    struct StepField* field = sf_new(STRING);
    field->data = value;
    *out = field;

cleanup:
    if (token) {
        free(token);
        token = NULL;
    }

    return rc;
}

/** @brief Reads the next token and verify expectation
 *
 * @param stp: File handle
 * @param expected: String to verify
 * @return ESUCCESS on success, -EUNEXP on unexpected token
 */
int step_expect(Step* stp, const char* expected)
{
    char* token = tstream_next(stp->ts);
    size_t len = strlen(token);
    int rc = ESUCCESS;

    int match = strncmp(token, expected, len);
    if (match != 0) {
        debug_printf("Error! Unexpected token '%s', expected '%s'\n", token, expected);
        rc = -EUNEXP;
    }

    free(token);
    token = NULL;

    return rc;
}

/** @brief Create a name for unamed entities
 *
 * @param out: struct StepField-pointer to recieve new name
 * @return ESUCCESS on success, -ENOMEM on allocation error
 */
int step_create_dummy_name(struct StepField** out)
{
    struct StepField* entity_name = sf_new(STRING);
    char dummy_name[] = "NONAME";
    size_t dummy_len = strlen(dummy_name) + 1;

    entity_name->data = calloc(1, dummy_len);
    if (!entity_name->data) {
        return -ENOMEM;
    }
    memcpy(entity_name->data, &dummy_name, dummy_len);

    *out = entity_name;
    return ESUCCESS;
}
