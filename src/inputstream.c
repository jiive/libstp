/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "inputstream.h"
#include "utils.h"

static char check_escape_sequence(IStream *is, char ch);
static char codepoint_lookup(IStream *is);
static void istream_ignore_to(IStream* is, const char ch);
static bool istream_is_comment(IStream* is);
static void istream_ignore_comment(IStream* is);
static void istream_toggle_stringmode(IStream* is);

#define CP_NUM_CODEPOINTS 6
struct CodePoint cp_table[CP_NUM_CODEPOINTS] = {
    {(char)0xE4, "X\\E4\0"}, /* 'ä' */
    {(char)0xE5, "X\\E5\0"}, /* 'å' */
    {(char)0xF6, "X\\F6\0"}, /* 'ö' */
    {(char)0xC4, "X\\C4\0"}, /* 'Ä' */
    {(char)0xC5, "X\\C5\0"}, /* 'Å' */
    {(char)0xD6, "X\\D6\0"}  /* 'Ö' */
};


/** @brief Opens an inputstream and sets default values.
 *  Reads whole file to RAM
 *
 * @param filename: Relative or absolute path to file to be read.
 * @return On success, pointer to inputstream handle. Returns NULL on failure.
 */
IStream* istream_open(const char* filename){
    IStream* is = calloc(1, sizeof(IStream));
    if (!is) {
        return NULL;
    }

    is->filesize = 0;
    is->file = read_file(filename, &is->filesize);

    if (!is->file) {
        free(is);
        return NULL;
    }

    // Set default values
    is->marker = 0;
    is->cursor = 0;
    is->current = 0;
    is->cline = 1;
    is->ccol = 1;
    is->in_string = 0;

    return is;
}

/** @brief Closes an inputstream and deallocates memory
 *
 * @param is: Handle to inputstream to be closed
 * @return: On success, true. On error NULL.
 */
int istream_close(IStream* is) {
    if (!is) {
        return 1;
    }

    if (is->file) {
        free(is->file);
        is->file = NULL;
    }

    is->marker = 0;
    is->cursor = 0;
    is->filesize = 0;
    is->cline = 0;
    is->ccol = 0;
    is->in_string = 0;

    free(is);
    is = NULL;
    return 1;
}

/** @brief Gets the next relevant character in stream
 *
 *  This function filters whitespace and linebreaks unless
 *  the file cursor is currently inside a string.
 *
 *  @param is: Handle to inputstream
 *  @return Char: next character in stream
 */
char istream_next(IStream* is) {
    char ch = 0;
    bool breakout = false;

    if (is->cursor >= is->filesize)
        return 0;

    if (is->current) {
        ch = is->current;
        is->current = 0;
    } else {
        while (!breakout) {
            ch = is->file[is->cursor];
            if (ch == '\0')
                return 0;
            is->cursor++;
            is->ccol++;

            switch (ch) {
            case '\n':
                is->cline++;
                is->ccol = 0;
                break;

            case '\r':
                break;

            case '\'':
                istream_toggle_stringmode(is);
                breakout = true;
                break;

            case '/':
                if (istream_is_comment(is))
                    istream_ignore_comment(is);
                else
                    breakout = true;
                break;

            case '\\':
                ch = check_escape_sequence(is, ch);
                breakout = true;
                break;

            case ' ':
                if (istream_in_string(is))
                    breakout = true;
                break;

            default:
                breakout = true;
                break;
            }
        }
    }

    return ch;
}

/** @brief This function peeks the next relevant character in stream
 *
 *  This function filters whitespace and linebreaks unless
 *  the file cursor is currently inside a string.
 *
 *  @param is: Handle to inputstream
 *  @return char: next character in stream
 */
char istream_peek(IStream* is) {
    if (!is->current)
        is->current = istream_next(is);

    return is->current;
}

/** @brief This function peeks the next character in stream
 *
 *  This function *does not* filter whitespace and linebreaks unlike
 *  \link istream_peek(IStream* is) \endlink
 *
 *  @param is: Handle to inputstream
 *  @return char: next character in stream
 */
char istream_raw_peek(IStream* is) {
    return is->file[is->cursor];
}

/** @brief Ignores characters in stream until and including the
 *  first occurence of ch
 *
 *  @param is: Handle to inputstream
 *  @param ch: Character to target
 *  @return NONE
 */
static void istream_ignore_to(IStream* is, const char ch) {
    do {
        ++is->cursor;
        // Handle line counter
        if (is->file[is->cursor] == '\n') {
            ++is->cline;
            is->ccol = 0;
        }
    } while (is->file[is->cursor] != ch);
    ++is->cursor;
}


/** @brief Determines if file cursor is inside quotes
 *
 *  @param is: Handle to inputstream
 *  @return True if inside quotes, else false
 */
bool istream_in_string(IStream* is) {
    return is->in_string;
}

/** @brief Called from istream_next() to determine if we have hit a comment
 *
 *  @param is: Handle to inputstream
 *  @return True if we hit a comment, else false
 */
static bool istream_is_comment(IStream* is) {
    return istream_raw_peek(is) == '*';
}

/** @brief Iterates over input until end-of-comment token
 *
 *  @param is: Handle to inputstream
 *  @return None
 */
static void istream_ignore_comment(IStream* is) {
    do {
        istream_ignore_to(is, '*');
    } while (istream_raw_peek(is) != '/');

    istream_next(is);
}

/** @brief Toggles stringmode when pasing quote-characters
 *
 *  @param is: Handle to inputstream
 *  @return None
 */
static void istream_toggle_stringmode(IStream* is) {
    is->in_string = !is->in_string;
}

/** @brief Dumps fileposition to stderr
 *
 *  @param is: Handle to inputstream
 *  @return None
 */
void istream_dump_state(IStream* is) {
    debug_printf("-Input State-\nCursor: %zu\nCurrent character: %c\nLine; %d\nColumn: %d\n",
            is->cursor,
            is->current,
            is->cline,
            is->ccol);
}

/** @brief Evaluate characters following a '\' character
 *
 *  @param is: Handle to inputstream
 *  @param ch: escape character
 *  @return Escaped character if valid, else continue as usual
 */
static char check_escape_sequence(IStream *is, char ch)
{
    /* \X might be followed by unicode code points */
    char cp = codepoint_lookup(is);
    if (cp)
        return cp;
    else
        return ch;
}

static char codepoint_lookup(IStream *is)
{
    size_t bytes = 5;
    char ch = 0;
    char *from_file = NULL;

    from_file = calloc(1, bytes);
    if (!from_file) {
        goto cleanup;
    }

    from_file = strncpy(from_file, &is->file[is->cursor], bytes-1);
    if (!from_file) {
        goto cleanup;
    }

    for (size_t i = 0; i < CP_NUM_CODEPOINTS; i++) {
        if (str_cmp(from_file, cp_table[i].bytes)) {
            ch = cp_table[i].ch;
            is->cursor += bytes-1;
            break;
        }
    }

cleanup:
    free(from_file);

    return ch;
}
