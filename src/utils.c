/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "error.h"
#include "step.h"
#include "step_entity.h"
#include "utils.h"

/** @brief Reads a whole file
 *
 *  @param file: absolute or relative path to file
 *  @param size: returning file size
 *  @return char*, pointer to file content on successful read, null on error
 *              size-parameter is set to 0 on error
 */
char* read_file(const char* file, size_t* size) {
    size_t filesize = 0;
    long ftell_size = 0;
    char* content = NULL;

    FILE *fp = fopen(file, "r");
    if (!fp) {
        debug_printf("Could not open file %s\n", file);
        filesize = 0;
        goto cleanup;
    }

    // Get file size
    fseek(fp, 0, SEEK_END);
    ftell_size = ftell(fp);
    if (ftell_size < 0) {
        goto cleanup;
    }
    filesize = (size_t)ftell_size;
    rewind(fp);

    // Allocate and fill string
    content = calloc(filesize+1, sizeof(char));
    if (!content) {
        debug_printf("Unable to allocate memory while reading file %s\n", file);
        filesize = 0;
        goto cleanup;
    }

    size_t read = fread(content, sizeof(char), filesize, fp);

    // Sanity check
    if (read != filesize) {
        debug_printf("Error while reading file %s\n", file);
        filesize = 0;
        free(content);
        content = NULL;
    }

cleanup:
    if (fp)
        fclose(fp);

    // Returns
    *size = filesize;
    return content;
}

/** @brief Builds a string from a list of values
 *
 *  @param field: StepField of list type
 *  @return Char*: Value representation as string. String might be null,
 *                indicating and empty string/list
 */
char* step_list_to_str(struct StepField* field) {
    if (!field) return NULL;
    if (field->type != LIST) return NULL;
    if (field->data == NULL) return NULL;

    Vector* list = field->data;
    size_t retsize = 0, retlen = 0, nodelen = 0;
    char* retstr = calloc(1, 1);
    char* nodeval = NULL;
    struct StepField* node = NULL;

    vector_reset(list);
    while ((node = vector_iterate(list))) {
        if (node->type == LIST) {
            nodeval = step_list_to_str(field);
            nodelen = strlen(nodeval) + 1;
            retlen = strlen(retstr);
            if ((retlen + nodelen) >= retsize) {
                retstr = realloc(retstr, retlen+nodelen);
                if (!retstr) {
                    free(nodeval);
                    return retstr;
                }
                retsize = retlen+nodelen;
            }
            strncat(retstr, nodeval, retsize);
            free(nodeval);
            nodeval = NULL;
        } else {
            retlen = strlen(retstr);
            nodelen = strlen((char*)field->data) + 1;
            retsize = retlen + nodelen;
            retstr = realloc(retstr, retsize);
            if (!retstr)
                break;

            retstr = strncat(retstr, (char*)node->data, retsize);
        }
    }

    return retstr;
}

/** @brief Test if string is alphanumeric, underscore '_' is allowed in string
 *
 *  @param val: string
 *  @return True if all characters in string is alphanumeric or underscore
 */
bool is_alnum(char* val)
{
    for (size_t i=0; i < strlen(val); i++) {
        if (isalnum(val[i]) == 0 && val[i] != '_')
            return false;
    }
    return true;
}

/** @brief Compare two null-terminated strings
 *
 *  @param one: string
 *  @param two: string
 *  @return True on match, false on non-match
 */
bool str_cmp(char* one, char* two)
{
    size_t olen = strlen(one);
    size_t tlen = strlen(two);

    if (olen != tlen)
        return false;

    for (size_t i=0; i < olen; i++) {
        if (one[i] != two[i])
            return false;
    }

    return true;
}

/** @brief Builds a string of num characters. String is always null terminated
 *
 *  NOTE: Don't forget to free string when done
 *
 *  @param ch: character to fill string
 *  @param num: number of spaces
 *  @return char*: null-termintated string
 */
char* generate_string(char ch, size_t num)
{
    size_t size = num + 1;
    char* str = calloc(1, size);
    memset(str, ch, size-1);

    return str;
}

/** @brief Prints a struct StepEntity, value list is printed recursivley
 *
 *  @param ent: Entity to print
 *  @return None
 */
void print_entity(struct StepEntity* ent)
{
    if (!ent) return;

    printf("Entity ID: %s\n", ent->id);
    printf("Entity type: %s\n", ent->type);
    printf("-- Values--: \n");
    print_field(ent->values, 0);
}

/** @brief Prints a Stepfield, lists are printed recursivley
 *
 *  @param field: Entity to print
 *  @param level: Indentation level
 *  @return None
 */
void print_field(struct StepField* field, size_t level)
{
    char* space = generate_string(' ', level*4);
    printf("%sField type: ", space);
    if (field->type == LIST) {
        printf("LIST\n");

        struct StepField *node = NULL;
        Vector *list = field->data;
        vector_reset(list);
        while ((node = vector_iterate(list))) {
            print_field(node, level+1);
        }
    } else {
        printf("STRING\n");
        printf("%sValue: %s\n", space, (char*)field->data);
    }

    free(space);
}

/** @brief Copies a NULL-terminated string
 *
 *  @param from: pointer to string NULL-terminated string to be copied
 *  @return Pointer to new NULL-terminated string
 */
char* copy_string(char *from)
{
    if (!from) return NULL; /* TODO Return empty string instead? */

    size_t len = strlen(from) + 1;
    char *newstr = calloc(len, sizeof(char));

    if (!newstr) return NULL;

    char *beginning = newstr;
    while ((*newstr++ = *from++));

    return beginning;
}

/** @brief Prints a struct Product
 *
 *  @param product: pointer to Product structure
 *  @return void
 */
void print_product(struct Product *product)
{
    if (!product) {
        printf("No more products to print\n");
        return;
    }

    char *line = generate_string('-', 15);
    printf("%s\n", line);

    printf("Partnumber: %s\nName: %s\nDescription: %s\n",
            product->partnumber,
            product->name,
            product->desc);

    free(line);
    line = NULL;
}

/** @brief Recursivly prints TreeItem structure
 *
 *  @param item: pointer to root TreeItem
 *  @param level: Call depth(for indentation)
 *  @return void
 */
void print_tree(struct TreeItem *item, size_t level)
{
    if (!item) return;

    char* space = generate_string(' ', level * 4);
    printf("%s|--------------------\n", space);
    printf("%s|Instance ID: %s\n", space, item->instance_id);
    printf("%s|Instance description: %s\n", space, item->instance_desc);
    printf("%s|Instance name: %s\n", space, item->instance_name);
    printf("%s|Step definition Part: %s\n", space, item->definition->partnumber);

    if (item->children) {
        struct TreeItem *node = NULL;
        size_t i = 0;
        while ((node = vector_get(item->children, i))) {
            print_tree(node, level + 1);
            i++;
        }
    }

    free(space);
    space = NULL;
}

/** @brief Writes datastructure to stdout in json format
 *
 *  @param stp: Step file handle
 *  @return void
 */
void dump_json(Step *stp)
{
    struct TreeItem *root_item = NULL;

    printf("{\n");
    dump_json_products(stp->products);
    dump_json_productdef(stp);

    printf("\"treeitems\":\n");
    vector_reset(stp->product_tree);
    while ((root_item = vector_iterate(stp->product_tree))) {
        dump_json_treeitems(root_item, 1);
    }

    printf("}\n");
}

/** @brief Writes Product datastructure to stdout in json format
 *
 *  @param v: Vector of struct Product
 *  @return void
 */
void dump_json_products(Vector *v)
{
    vector_reset(v);
    struct Product *p = vector_iterate(v);
    bool iter = true;
    if (!p) return;

    printf("\"products\":\n{\n");
    while (iter) {
        printf("\t\"%s\": {\"partnumber\": \"%s\", \"name\": \"%s\", \"desc\": \"%s\"}",
                p->step_id,
                p->partnumber,
                p->name,
                p->desc);
        p = vector_iterate(v);
        if (!p) iter = false;
        else printf(",\n");
    }
    printf("\n},\n");
}

/** @brief Writes ProductDefinition structure to stdout in json format
 *
 *  @param v: Vector of struct Product
 *  @return void
 */
void dump_json_productdef(Step *stp)
{
    vector_reset(stp->definitions);
    struct ProductDefinition *p = vector_iterate(stp->definitions);
    if (!p) return;
    bool iter = true;

    printf("\"definitions\":\n{\n");
    while (iter) {
        char *pid = pr_find_product(stp->formations, p->reference);
        printf("\t\"%s\": {\"partnumber\": \"%s\", \"desc\": \"%s\"" \
               ", \"product_master\": %s}",
               p->step_id,
               p->partnumber,
               p->desc,
               pid);
        p = vector_iterate(stp->definitions);
        if (!p) iter = false;
        else printf(",\n");
    }
    printf("\n},\n");
}

/** @brief Recursivly prints struct TreeItem in json format
 *
 *  @param item: TreeItem to print
 *  @param level: Indentation level
 *  @return void
 */
void dump_json_treeitems(struct TreeItem* item, size_t level)
{
    if (!item) return;

    struct TreeItem *child = NULL;
    char* space = generate_string('\t', level);


    printf("%s{\"instance_id\": \"%s\",\n", space, item->instance_id);
    printf("%s\"instance_name\": \"%s\",\n", space, item->instance_name);
    printf("%s\"instance_desc\": \"%s\",\n", space, item->instance_desc);
    printf("%s\"product_master_id\": \"%s\",\n", space, item->product_id);
    printf("%s\"product_definition_id\": \"%s\"", space, item->definition->step_id);

    if (item->children) {
        printf(",\n%s\"children\": [\n", space);
        for (size_t i = 0; i < vector_count(item->children); i++) {
            child = vector_get(item->children, i);
            dump_json_treeitems(child, level + 1);
            /* Print object separator unless we are at last object */
            if (i != vector_count(item->children)-1)
                printf("%s,\n", space);
            else
                break;

        }
        printf("%s]\n", space);
    }

    printf("\n%s}\n", space);

    free(space);
    space = NULL;
}
