/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef STEP_LIST_H
#define STEP_LIST_H

#include <stdbool.h>
#include "error.h"
#include "step_field.h"

struct StepList {
    struct StepField* field;
    struct StepList* next;
};

struct StepList* sl_new();
int sl_free(struct StepField* list);

bool sl_append(struct StepField* list, struct StepField* field);
struct StepList* sl_index(struct StepField* list, size_t idx);
struct StepList* sl_last(struct StepField* list);
int sl_copy(struct StepField* list, struct StepField** out);

#endif
