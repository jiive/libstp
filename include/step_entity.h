/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef STEP_ENTITY_H
#define STEP_ENTITY_H

#include "error.h"
#include "step.h"
#include "step_field.h"

struct StepEntity{
    char* id;
    char* type;
    struct StepField* values; /* Vector* of type StepField* */
    struct StepEntity* next;
};

int se_new(struct StepEntity** out, char* id, char* type, struct StepField* values);
int se_append(struct StepEntity** last, struct StepEntity* node);
void se_free(struct StepEntity* ent);
int se_find_type(struct StepEntity* first, char* type, struct StepEntity** out);
struct StepEntity* se_copy(struct StepEntity* orig);

#endif
