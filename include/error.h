/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>
#include "config.h"

/* DEBUG is defined in config.h */
/* Simple string print */
#define debug_print(STR)\
    do { if (DEBUG) fprintf(stderr, "%s:%d:%s(): %s\n", __FILE__, \
            __LINE__, __func__, STR); } while (0)

/* Allow format string */
#define debug_printf(fmt, ...) \
    do { if (DEBUG) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
            __LINE__, __func__, __VA_ARGS__); } while (0)

/* Return codes */
#define ESUCCESS 0
#define EUNEXP 500 /* Unexpected input/token */

extern const char* LOG_FILENAME;

#endif
