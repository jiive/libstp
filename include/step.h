/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef STEP_H
#define STEP_H

#include <stdbool.h>
#include <vector.h>

#include "error.h"
#include "step_entity.h"
#include "step_field.h"
#include "tokenstream.h"

typedef struct {
    TStream* ts;
    size_t num_entities;
    Vector *header;                 /* Type struct StepEntity */
    struct StepEntity* data_first;  /* List of entities in data seciton */
    struct StepEntity* data_last;
    Vector *products;               /* Type struct Product */
    Vector *definitions;
    Vector *formations;
    Vector *usage;
    /* Vector tree_items is responsible for freeing all treeitems,
     * *product_tree should not be freed as is can contain multiple
     * pointers pointing to the same memory, causing double-free */
    Vector *tree_items;
    Vector *product_tree;
} Step;

/* Top level functions */
int step_open(Step** stp, const char* filename);
void step_close(Step** stp);
int step_read_header(Step* stp);
int step_header_entity(Step* stp, char* name, struct StepEntity** out);
int step_read_data(Step* stp);
int step_parse(Step* stp);
bool step_verify_iso(Step* stp);

/* Process tokens */
int step_get_line(Step* stp, struct StepField** out);
int step_get_string(Step* stp, struct StepField** out);
int step_get_id(Step* stp, struct StepField** out);
int step_get_token(Step* stp, struct StepField** out);
int step_get_list(Step* stp, struct StepField** out);
char* step_list_to_str(struct StepField* field);
int step_get_value(Step* stp, struct StepField** out);
int step_expect(Step* stp, const char* expected);
int step_create_dummy_name(struct StepField** out);

#endif
