/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef UTILS_H
#define UTILS_H

#include "product.h"
#include "step.h"
#include "step_entity.h"
#include "treeitem.h"

char* read_file(const char* file, size_t* size);
bool is_alnum(char* val);
bool str_cmp(char* one, char* two);
char* generate_string(char ch, size_t num);
void print_entity(struct StepEntity* ent);
void print_field(struct StepField* field, size_t level);
void print_product(struct Product *prod);
void print_tree(struct TreeItem *item, size_t level);
char* copy_string(char *from);
void dump_json(Step *stp);
void dump_json_products(Vector *products);
void dump_json_productdef(Step *stp);
void dump_json_treeitems(struct TreeItem *item, size_t level);
/* gchar* iso_to_utf8(char* str); */

#endif
