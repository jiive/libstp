/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef STEP_FIELD_H
#define STEP_FIELD_H

typedef enum {STRING, LIST} StepType;

struct StepField{
    StepType type;
    void* data;
} ;

struct StepField* sf_new();
void sf_free(struct StepField* field);
void sf_copy(struct StepField *orig, struct StepField **copy);


#endif
