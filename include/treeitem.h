/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef TREEITEM_H
#define TREEITEM_H

#include <vector.h>
#include "config.h"

/* TreeItem is an instanciation of ProductUsage */
struct TreeItem {
    struct ProductDefinition *definition;
    struct ProductDefinition *parent;
    struct Product           *master;
    Vector                   *children;
    char                     *instance_desc; /* instance NEXT_ASSEMBLY_USAGE_OCCURRENCE */
    char                     *instance_id;   /* instance NEXT_ASSEMBLY_USAGE_OCCURRENCE */
    char                     *instance_name; /* instance NEXT_ASSEMBLY_USAGE_OCCURRENCE */
    char                     *product_id;    /* related PRODUCT */
};

struct TreeItem*        ti_new(char *id,
                                char *name,
                                char *desc,
                                char *product_id,
                                struct ProductDefinition *definition,
                                struct ProductDefinition *parent,
                                struct Product *product);
void                    ti_free(struct TreeItem *node);
int                     ti_append_child(struct TreeItem *item,
                                        struct TreeItem *child);
struct TreeItem*        ti_find_id(Vector *list, char* id);
int                     build_product_tree(Step *stp);
int                     build_tree_items(Step *stp,
                                         Vector *children,
                                         struct TreeItem **items);


#endif
