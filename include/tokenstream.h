/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#include <string.h>
#include "inputstream.h"

#ifndef TOKENSTREAM_H
#define TOKENSTREAM_H

static char const DELIM_HASH[] = "#\0";
static char const DELIM_QUOTE[] = "\'\0";
static char const DELIM_LISTSTART[] = "(\0";
static char const DELIM_LISTEND[] = ")\0";
static char const DELIM_VALUE_SEPARATOR[] = ",\0";
static char const DELIM_LINEEND[] = ";\0";
static char const DELIM_ASSIGNMENT[] = "=\0";
static char const DELIMS[] = "(),'#;=\0";

// This constant determin the size of token buffer, the buffer
// size is also increase by tstream_BUFFER_SIZE amount as needed during parsing
// Don't set this value below 2
static size_t const TS_BUFFER_SIZE = 10;

typedef struct {
    IStream* is;
    char* current;
    char* buffer;
    size_t bufsize;
} TStream;

TStream* tstream_open(const char* filename);
int tstream_close(TStream* ts);
char* tstream_next(TStream* ts);
char* tstream_peek(TStream* ts);
char* tstream_get_token(TStream* ts);
size_t tstream_build_token(TStream* ts);
size_t tstream_get_delim(TStream* ts);
bool tstream_is_numeric(const char* token);
bool tstream_is_alnum(const char* token);
bool tstream_is_delim(TStream* ts, const char ch);
bool tstream_is_eol(const char ch);
int tstream_increase_buffer(TStream* ts);

#endif
