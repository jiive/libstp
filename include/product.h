/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef PRODUCT_H
#define PRODUCT_H

#include <stdlib.h>
#include "step.h"
#include "step_entity.h"

/* PRODUCT */
struct Product {
    char                *desc;
    char                *name;
    char                *partnumber;
    char                *step_id;
};

/* NEXT_ASSEMBLY_USAGE_OCCURENCE */
struct ProductUsage {
    char                     *child;
    char                     *desc;
    char                     *id;
    char                     *name;
    char                     *parent;
    struct ProductDefinition *definition;
};

/* PRODUCT_DEFINITION_FORMATION */
struct ProductReference {
    char                    *desc;
    char                    *id;
    char                    *product_id;
    char                    *step_id;
};

/* PRODUCT_DEFINITION */
struct ProductDefinition {
    Vector                   *children; /* Of struct ProdctUsage */
    char                     *context;
    char                     *desc;
    bool                     is_child;
    char                     *partnumber;
    char                     *reference; /* to PRODUCT_DEFINITION_FORMATION */
    char                     *step_id;
};

int                         process_product_entities(Step *stp);
/* struct Product funcitons */
struct Product*             product_new(struct StepEntity *ent);
void                        product_free(struct Product *node);
Vector*                     product_convert_entities(struct StepEntity *node);
struct Product*             product_find(Vector *products, char* id);

/* struct ProductUsage functions */
struct ProductUsage*        pu_new(struct StepEntity *ent);
void                        pu_free(struct ProductUsage *node);
Vector*                     pu_convert_entities(struct StepEntity *node);

/* struct ProductReference functions */
struct ProductReference*    pr_new(struct StepEntity *entity);
void                        pr_free(struct ProductReference* node);
Vector*                     pr_convert_entities(struct StepEntity *node);
char*                       pr_find_product(Vector *references,
                                            char *id);

/* struct ProductDefinition functions */
struct ProductDefinition*   pd_new(struct StepEntity *ent);
void                        pd_free(struct ProductDefinition* node);
Vector*                     pd_convert_entities(struct StepEntity *node);
struct ProductDefinition*   pd_find_by_id(Vector *definitions, char* id);
int                         pd_append_child(struct ProductDefinition *def,
                                            struct ProductUsage *child);
int                         establish_relationships(Step *stp);

#endif
