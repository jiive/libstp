/* libstp
 * Copyright (C) 2019-2020  Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option)  any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */

#ifndef INPUTSTREAM_H
#define INPUTSTREAM_H

#include <stdbool.h>

typedef struct IStream {
    size_t marker;
    size_t cursor;
    size_t filesize;
    char* file;
    char current;
    int cline, ccol;
    int in_string;
} IStream;

#define CP_NUM_BYTES 7

struct CodePoint {
    char ch;
    char bytes[CP_NUM_BYTES];
};

IStream* istream_open(const char* filename);
int istream_close(IStream* is);
char istream_next(IStream* is);
char istream_peek(IStream* is);
char istream_raw_peek(IStream* is);
void istream_reset(IStream* is);
void istream_dump_state(IStream* is);
char* istream_read(IStream* is);
bool istream_in_string(IStream* is);

#endif
