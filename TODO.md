# TODO
* Replace StepEntity linked list in Step struct with something more appropriate
* Clean up all calls to free(), no if (var) {free(var);} if all pointers are
initialized to NULL.
* Add documentation for all structs and write down notes about the memory
model used
* Add handlers for other STEP sections other than HEADER and DATA
* Add detection for STEP-xml
* Document library usage with examples
* Write test units
* Remove StepList
* Use separate(non tracked) script for running valgrind
* Cleanup cmake, verify install routine

# DONE
* Move to CMake instead of GNU make
* Rename functions for consistency, *_init => *_new and *_delete => *_free
* Fix warnings due to new compilation flags
* Convert all RC_* enums to errno instead
* Breakout Vector to separate lib
* Clean up inputstream.c:codepoint_lookup()
* [REMOVED] Document gui_utils.c:iso_to_utf8()
* Remove all calls to exit() in library code
